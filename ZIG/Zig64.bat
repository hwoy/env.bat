@echo off

set GIT=C:\TEMP\msys64\usr
set TOOLCHAIN_PATH=I:\Coding
set DRMEMORY=I:\Coding\DrMemory-Windows-2.5.0


SET COMPILERDIR=%TOOLCHAIN_PATH%\zig

SET ZIG_LOCAL_CACHE_DIR=%USERPROFILE%\Desktop\zig_cache
SET ZIG_GLOBAL_CACHE_DIR=%ZIG_LOCAL_CACHE_DIR%

set PATH=%COMPILERDIR%;%DRMEMORY%\bin64;%GIT%\bin;%PATH%

cmd.exe /K title Zig 64 environment