#!/bin/sh

CODE=/i/Coding

wget -qO- $(wget -qO- https://ziglang.org/download/index.json | grep zig-windows-x86_64 | head -n1 | tr -d '"", '  | cut -c 9-) | bsdtar -xvf- -C $CODE

ZIG_DIR=zig-dev

rm -rf $CODE/$ZIG_DIR

mv $CODE/zig-windows* $CODE/$ZIG_DIR

read -p "Press any key to continue"

