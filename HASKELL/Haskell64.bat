@echo off

set GIT=C:\TEMP\msys64\usr
set TOOLCHAIN_PATH=I:\Coding



set GHCUP_INSTALL_BASE_PREFIX=%TOOLCHAIN_PATH%\ghc

set HASKELL=%GHCUP_INSTALL_BASE_PREFIX%\ghcup
set CABAL_DIR=%HASKELL%

set PATH=%HASKELL%\bin;%GIT%\bin;%PATH%

cmd.exe /K title Haskell 64 Environment
