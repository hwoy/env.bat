#!/bin/sh

export default_toolchain=$(rustup default)
export tool_chain=$(echo $default_toolchain | cut - -d ' ' -f 1)
export target_arch=$(echo $tool_chain | cut - -d '-' -f 2-)

rm -rv $RUSTUP_HOME/toolchains/$tool_chain/lib/rustlib/$target_arch/bin
rm -rv $RUSTUP_HOME/toolchains/$tool_chain/lib/rustlib/$target_arch/lib/self-contained