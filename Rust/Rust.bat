@echo off


set GIT_HOME=C:\TEMP\msys64\usr

CALL Rust_Env.bat

set PATH=%PATH%;%GIT_HOME%\bin

rustup default
cmd.exe /K title  Rust environment
