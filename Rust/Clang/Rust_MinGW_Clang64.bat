@echo off

:SET_ENV

set MINGW_HOME=C:\Temp\msys64

set DRMEMORY_HOME=C:\Coding\DrMemory-Windows-2.6.0


set RUSTUP_HOME=c:\Coding\Rust
set CARGO_HOME=%RUSTUP_HOME%\Cargo

set COMPILERDIR=%CARGO_HOME%

set PATH=%COMPILERDIR%\bin;%DRMEMORY_HOME%\bin64;%PATH%

:SET_TOOLCHAIN

FOR /F "tokens=* USEBACKQ" %%F IN (`rustup default`) DO (
SET var=%%F
)

if "%var%"=="stable-x86_64-pc-windows-gnu (default)" (
set MINGW=clang64
goto :SET_RUST_MSYS2
)

if "%var%"=="stable-i686-pc-windows-gnu (default)" (
set MINGW=clang32
goto :SET_RUST_MSYS2
)

:ERROR_EXIT

echo Rust toolchain not match !
@pause
exit 1

:SET_RUST_MSYS2

%MINGW_HOME%\msys2_shell.cmd -%MINGW% -here -use-full-path