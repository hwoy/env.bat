#!/bin/sh

export PKG_CONFIG_LIBDIR=$PKG_CONFIG_PATH
export PKG_CONFIG_SYSROOT_DIR=/clang32
export CARGO_BUILD_TARGET=i686-pc-windows-gnu
export PATH=$PATH:/clang64/bin
alias rustc32="rustc --target i686-pc-windows-gnu"
