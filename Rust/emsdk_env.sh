#!/bin/sh

#set env

export CARGO_BUILD_TARGET=wasm32-unknown-emscripten

export TOOLCHAIN_PATH=/I\\Coding

export EMSDK_DIR=$TOOLCHAIN_PATH\\emsdk
export WABT_DIR=$TOOLCHAIN_PATH\\wabt

export EMCC_NODE_BIN=$EMSDK_DIR\\node\\16.20.0_64bit\\bin
export EMCC_JAVA_BIN=$EMSDK_DIR\\java\\8.152_64bit\\bin
export EMCC_PYTHON_BIN=$EMSDK_DIR\\python\\3.9.2-nuget_64bit
export EMCC_WASM_BIN=$EMSDK_DIR\\upstream\\bin
export EMCC_EMSCRIPTEN_BIN=$EMSDK_DIR\\upstream\\emscripten

export EMSDK_PYTHON=$EMCC_PYTHON_BIN\\python.exe


export PATH=$EMCC_NODE_BIN:$EMCC_JAVA_BIN:$EMCC_PYTHON_BIN:$EMCC_WASM_BIN:$EMCC_EMSCRIPTEN_BIN:$PATH

export PATH=$PATH:$WABT_DIR\\bin


#set path ot emsdk

export EMSDK_INCLUDE_DIR=$EMCC_EMSCRIPTEN_BIN\\cache\\sysroot\\include


export C_INCLUDE_PATH=$EMSDK_INCLUDE_DIR

