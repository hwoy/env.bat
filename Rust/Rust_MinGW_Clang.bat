@echo off

:SET_ENV

CALL Rust_Env.bat

:SET_TOOLCHAIN

FOR /F "tokens=* USEBACKQ" %%F IN (`rustup default`) DO (
SET var=%%F
)

if "%var%"=="stable-x86_64-pc-windows-gnu (default)" (
set MINGW=clang64
goto :SET_RUST_MSYS2
)

if "%var%"=="stable-i686-pc-windows-gnu (default)" (
set MINGW=clang32
goto :SET_RUST_MSYS2
)

:ERROR_EXIT

echo Rust toolchain not match !
@pause
exit 1

:SET_RUST_MSYS2

%MINGW_HOME%\msys2_shell.cmd -%MINGW% -here -use-full-path