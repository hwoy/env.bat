#!/bin/sh

export TOOLCHAIN_PATH=/I\\Coding


export WASMER_BIN_DIR=$TOOLCHAIN_PATH\\wasmer\\bin

export WASMTIME_BIN_DIR=$TOOLCHAIN_PATH\\wasmtime\\bin

export WASI_DIR=$TOOLCHAIN_PATH\\wasi-sdk


export CARGO_BUILD_TARGET=wasm32-wasi


export WASI_INCLUDE_DIR=$WASI_DIR\\\share\\wasi-sysroot\\include


export PATH=$WASI_DIR\\bin:$WASMER_BIN_DIR:$WASMTIME_BIN_DIR:$PATH

export C_INCLUDE_PATH=$WASI_INCLUDE_DIR

