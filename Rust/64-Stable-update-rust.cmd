@echo off

REM Requires wget
REM https://eternallybored.org/misc/wget

set DHOST=x86_64-pc-windows-gnu

CALL Rust_Env.bat

set RUSTUP_HOME=%TOOLCHAIN_PATH%\Rust

set TOOLCHAIN=stable

set CARGO_HOME=%RUSTUP_HOME%\Cargo
set RUSTUPFLAGS=--no-modify-path -y -v

set RUSTUP_INIT=rustup-init.exe
set RUSTUP_URL="https://static.rust-lang.org/rustup/dist/%DHOST%/%RUSTUP_INIT%"

if not exist %RUSTUP_INIT% (
goto :DO_RUST_INIT
)
goto :DO_RUSTUP


:DO_RUST_INIT
if not exist wget.exe (
goto :DO_WGET_NOT_EXIST
)
set RUSTUP_URL="https://static.rust-lang.org/rustup/dist/x86_64-pc-windows-msvc/%RUSTUP_INIT%"
wget.exe -4 %RUSTUP_URL%


:DO_RUSTUP
%RUSTUP_INIT% %RUSTUPFLAGS% --default-host %DHOST% --default-toolchain=%TOOLCHAIN%
@pause
exit 0


:DO_WGET_NOT_EXIST
echo.
echo Requires wget
echo https://eternallybored.org/misc/wget
echo.
@pause
exit 1
